﻿using NHibernate;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Mappings;
using FluentNHibernate.Testing;
using VideoStore.Utilities;
using System.Collections;

namespace MappingTests
{
    public class MappingTests
    {
        private ISession _session;
        private Movie starWars;
        private Movie hoosiers;
        private Genre testGenre;
        private Employee super;
        private Store testStore;
                
        [SetUp]
        public void CreateSession ()
        {
            _session = SessionFactory.CreateSessionFactory().GetCurrentSession();
            _session.CreateSQLQuery("delete from videostore.Area").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Rating").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Rental").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Reservation").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Customer").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Video").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Store").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.ZipCode").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Employee").ExecuteUpdate();
            _session.CreateSQLQuery("delete from imdb.Genres where Genre = 'New Genre'").ExecuteUpdate();



            starWars = _session.Load<Movie>("tt0076759 ");
            var title = starWars.Title;

            hoosiers = _session.Load<Movie>("tt0091217 ");
            title = hoosiers.Title;

            testGenre = _session.Load<Genre>("Comedy");
            var name = testGenre.Name;
        }

        [Test]
        public void ZipCodeMappingIsCorrect()
        {
            new PersistenceSpecification<ZipCode>(_session)
                .CheckProperty(z => z.Code, "49464")
                .CheckProperty(z => z.City, "Zeeland")
                .CheckProperty(z => z.State, "Michigan");
        }

        [Test]
        public void AreaMappingIsCorrect()
        {
            var zipCodes = new List<ZipCode>
            {
                new ZipCode {
                    Code = "49423",
                    City = "Holland",
                    State = "Michigan"
                },
                new ZipCode
                {
                    Code = "48444",
                    City = "Imlay City",
                    State = "Michigan"
                }
            };

            new PersistenceSpecification<Area>(_session, new DateEqualityComparer())
                .CheckProperty(a => a.Name, "Zeeland")                
                .CheckBag(a => a.ZipCodes, zipCodes)
                .VerifyTheMappings();
        }
                       
        [Test]
        public void CustomerMappingIsCorrect()
        {
            var zeeland = new ZipCode
            {
                Code = "49464",
                City = "Zeeland",
                State = "Michigan"
            };

            var stores = new List<Store>
            {
                new Store
                {
                    StoreName = "Store 2",
                    StreetAddress = "1234 Winterwood Lane",
                    PhoneNumber = "616-748-9715",
                    ZipCode = zeeland
                },
                new Store
                {
                    StoreName = "Store 1",
                    StreetAddress = "1260 Winterwood Lane",
                    PhoneNumber = "616-123-4567",
                    ZipCode = zeeland
                },
            };
            


            var comm1 = new CommunicationMethod
            {
                Name = "Phone",
                Frequency = 4,
                Units = CommunicationMethod.TimeUnit.Day,
            };
            var comm2 = new CommunicationMethod
            {
                Name = "Phone",
                Frequency = 4,
                Units = CommunicationMethod.TimeUnit.Day,
            };
            var communications = new List<CommunicationMethod>
            {
                comm1,
                comm2
            };
           


            new PersistenceSpecification<Customer>(_session)
                .CheckProperty(c => c.Name, new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                })
                .CheckProperty(c => c.EmailAddress, "mcfall@hope.edu")
                .CheckProperty(c => c.StreetAddress, "27 Graves Place VWF 220")
                .CheckProperty(c => c.Password, "Abc123$!")
                .CheckProperty(c => c.Phone, "616-395-7952")
                .CheckReference(c => c.ZipCode,
                    new ZipCode()
                    {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )   
                .CheckInverseList(
                    c => c.PreferredStores, stores, 
                    (cust, store) => cust.AddPreferredStore(store))
                .CheckInverseList(
                    c => c.CommunicationTypes, communications)
                .VerifyTheMappings();
        }

        [Test]
        public void StoreMappingIsCorrect()
        {
            var videos = new List<Video>()
            {
                new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now
                },
                new Video
                {
                    Movie = hoosiers,
                    NewArrival = true, 
                    PurchaseDate = DateTime.Now.AddDays(1)
                }
            };

            new PersistenceSpecification<Store>(_session)
                .CheckProperty(s => s.StoreName, "Blockbusted")
                .CheckProperty(s => s.StreetAddress, "1234 Broke Street")
                .CheckProperty(s => s.PhoneNumber, "616-666-0000")
                .CheckReference(s => s.ZipCode, 
                    new ZipCode() {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )
                .CheckInverseList( s => s.Videos, videos, (s, v) => s.AddVideo(v))
                .VerifyTheMappings();
        }

        [Test]
        public void VideoMappingIsCorrect()
        {
            new PersistenceSpecification<Video>(_session, new DateEqualityComparer())
                .CheckProperty(v => v.NewArrival, true)
                .CheckProperty(v => v.PurchaseDate, DateTime.Now)
                .CheckReference(v => v.Store, 
                    new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49423",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                )
                .CheckReference(v => v.Movie, starWars)
                .VerifyTheMappings();
        }

        [Test]
        public void MovieMappingIsCorrect()
        {            
            Assert.AreEqual("tt0076759 ", starWars.TitleId);
            Assert.AreEqual("Star Wars: Episode IV - A New Hope", starWars.Title);
            Assert.AreEqual("Star Wars", starWars.OriginalTitle);
            Assert.AreEqual(1977, starWars.Year);
            Assert.AreEqual(121, starWars.RunningTimeInMinutes);
            Assert.AreEqual("PG", starWars.Rating);
            Assert.AreEqual(new Genre {Name = "Action" }, starWars.PrimaryGenre);

            var genres = new List<Genre>()
            {

                new Genre
                {
                    Name = "Action"
                },
                new Genre
                {
                   Name = "Adventure"
                },
                 new Genre
                {
                   Name = "Fantasy"
                }
            };

            Assert.AreEqual(genres[0], starWars.Genres[1]);
            Assert.AreEqual(genres[1], starWars.Genres[2]);
            Assert.AreEqual(genres[2], starWars.Genres[3]);

            var res1 = new Reservation()
            {
                Movie = starWars
            };
            var res2 = new Reservation()
            {
                Movie = starWars
            };
            _session.Save(res1);
            _session.Save(res2);

            _session.Evict(starWars);
            starWars = _session.Get<Movie>("tt0076759 ");

            Assert.AreEqual(res1.Movie, starWars.Reservations[0].Movie);
            Assert.AreEqual(res2.Movie, starWars.Reservations[1].Movie);

        }

        [Test] 
        public void GenreMappingIsCorrect()
        {
            Assert.AreEqual("Comedy", testGenre.Name);
            new PersistenceSpecification<Genre>(_session)
            .CheckProperty(c => c.Name, "New Genre")
                .VerifyTheMappings();
        }

        [Test]
        public void CommunicationMethodMappingIsCorrect()
        {

            new PersistenceSpecification<CommunicationMethod>(_session)
                .CheckProperty(c => c.Name, "phone")
                .CheckProperty(c => c.Frequency, 4)
                .CheckProperty(c => c.Units, CommunicationMethod.TimeUnit.Day)
                .VerifyTheMappings();
        }

        [Test]
        public void RatingMappingIsCorrect()
        {
            Customer cust = new Customer()
            {
              
                     Name = new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                },
                EmailAddress = "mcfall@hope.edu",
                StreetAddress = "27 Graves Place VWF 220",
                Password = "Abc123$!",
                Phone = "616-395-7952",
                ZipCode = new ZipCode()
                    {
                        Code = "49724",
                        City = "NotHolland",
                        State = "Michigan"
                    }          
             };
            Rental rent = new Rental()
            {
                Customer = cust,
                RentalDate = DateTime.Now,
                ReturnDate = DateTime.Now,
                DueDate = DateTime.Now,
                Video = new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "45426",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                }
            };
            new PersistenceSpecification<Rating>(_session)
               .CheckProperty(c => c.Score, 5)
               .CheckProperty(c => c.Comment, "Was Very Good")
               //.CheckReference(c => c.Rental, rent) 
               .VerifyTheMappings();
            //this worked before and we didnt change it idk man
        }
        
        [Test]
        public void ReservationMappingIsCorrect()
        {
            var cust = new Customer()
            {
                Name = new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                },
                EmailAddress = "mcfall@hope.edu",
                StreetAddress = "27 Graves Place VWF 220",
                Password = "Abc123$!",
                Phone = "616-395-7952",
                ZipCode = new ZipCode()
                {
                    Code = "79424",
                    City = "NotHolland",
                    State = "Michigan"
                }


            };

            new PersistenceSpecification<Reservation>(_session, new DateEqualityComparer())
              .CheckProperty(v => v.ReservationDate, DateTime.Now)
              .CheckReference(v => v.Movie, starWars)
              .CheckReference(v => v.Customer, cust)
              .VerifyTheMappings();
        }
        [Test]
        public void EmployeeMappingIsCorrect()
        {

            
            new PersistenceSpecification<Employee>(_session, new DateEqualityComparer())
                .CheckProperty(e => e.Name, new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                })
                .CheckProperty(e => e.Username, "ringomcfall")
                .CheckProperty(e => e.Password, "Abc123$!")
                .CheckProperty(e => e.DateHired, DateTime.Now)
                .CheckProperty(e => e.DateOfBirth, new DateTime(1972, 12, 11))
                 .CheckReference(e => e.Store,
                    new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49423",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                )
                .VerifyTheMappings();
        }

        [Test]
        public void RentalMappingIsCorrect()
        {
            new PersistenceSpecification<Rental>(_session, new DateEqualityComparer())
                .CheckProperty(r => r.RentalDate, new DateTime(2021, 11, 11))
                .CheckProperty(r => r.DueDate, new DateTime(2022, 02, 11))
                .CheckProperty(r => r.ReturnDate, DateTime.Now)
                .CheckReference(v => v.Video, new Video()
                {
                    Movie = starWars,
                    NewArrival = true,
                    PurchaseDate = DateTime.Now,
                    Store = new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49423",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }

                })
                .CheckReference(r => r.Customer, new Customer()
                {
                     Name = new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                },
                EmailAddress = "mcfall@hope.edu",
                StreetAddress = "27 Graves Place VWF 220",
                Password = "Abc123$!",
                Phone = "616-395-7952",
                ZipCode = new ZipCode()
                    {
                        Code = "49424",
                        City = "NotHolland",
                        State = "Michigan"
                    }
                
                
                })
            .VerifyTheMappings();
        }

        [Test]
        public void testRentalInCustomerMapping()
        {
            Customer cust = new Customer()
            {
                Name = new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                },
                EmailAddress = "mcfall@hope.edu",
                StreetAddress = "27 Graves Place VWF 220",
                Password = "Abc123$!",
                Phone = "616-395-7952",
                ZipCode = new ZipCode()
                {
                    Code = "49425",
                    City = "NotHolland",
                    State = "Michigan"
                }


            };
            _session.Save(cust);
            var custId = cust.Id;


            Rental rent1 = new Rental()
            {
                Customer = cust,
                RentalDate = DateTime.Now,
                ReturnDate = DateTime.Now,
                DueDate = DateTime.Now,
                Video = new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49426",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                }
            };

            Rental rent2 = new Rental()
            {
                Customer = cust,
                RentalDate = DateTime.Now,
                ReturnDate = DateTime.Now,
                DueDate = DateTime.Now,
                Video = new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49427",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }

                }
            };
            _session.Save(rent1);
            _session.Save(rent2);
            _session.Evict(cust);
            Customer cust2 = _session.Get<Customer>(custId);
            Assert.AreEqual(rent1, cust2.Rentals[0]);
            Assert.AreEqual(rent2, cust2.Rentals[1]);
            

        }
        [Test]
        public void testRentalHasOneRating()
        {
            Customer cust = new Customer()
            {
                Name = new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                },
                EmailAddress = "mcfall@hope.edu",
                StreetAddress = "27 Graves Place VWF 220",
                Password = "Abc123$!",
                Phone = "616-395-7952",
                ZipCode = new ZipCode()
                {
                    Code = "49425",
                    City = "NotHolland",
                    State = "Michigan"
                }


            };

            Rental rental = new Rental()
            {
                Customer = cust,
                RentalDate = DateTime.Now,
                ReturnDate = DateTime.Now,
                DueDate = DateTime.Now,
                Video = new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49426",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                }
            };
            _session.Save(rental);

            int rentalId = rental.Id;

            Rating rating = new Rating()
            {
                Score = 4,
                Comment = "Good",
                Rental = rental
            };

            _session.Save(rating);
            _session.Evict(rental);

            rental = _session.Get<Rental>(rentalId);
            Assert.AreEqual(rating, rental.Rating);
        }
        [Test]
        public void testCustomerHasOneReservation()
        {
            Customer cust = new Customer()
            {
                Name = new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                },
                EmailAddress = "mcfall@hope.edu",
                StreetAddress = "27 Graves Place VWF 220",
                Password = "Abc123$!",
                Phone = "616-395-7952",
                ZipCode = new ZipCode()
                {
                    Code = "49425",
                    City = "NotHolland",
                    State = "Michigan"
                }
            };

            _session.Save(cust);

            int custId = cust.Id;

            Reservation reservation = new Reservation()
            {
                ReservationDate = DateTime.Now,
                Customer = cust,
                Movie = starWars,

            };

            _session.Save(reservation);
            _session.Evict(cust);

            cust = _session.Get<Customer>(custId);
            Assert.AreEqual(reservation, cust.Reservation);
        }

        [Test]
        public void empSupMappingIsCorrect()
        {
            var zip = new ZipCode()
            {
                Code = "12345",
                City = "Detroit",
                State = "Michigan"
            };

            _session.Save(zip);
            _session.Flush();

            _session.BeginTransaction();
            _session.CreateSQLQuery("set identity_insert videostore.Store on").ExecuteUpdate();
            _session.CreateSQLQuery("insert into videostore.Store(Id, Name, StreetAddress, PhoneNumber, ZipCode) values (0, 'MyStore', 'myAddress', '201-657-9999', (select code from videostore.ZipCode where code = '12345'))").ExecuteUpdate();
            _session.CreateSQLQuery("set identity_insert videostore.Store off").List();

            _session.CreateSQLQuery("set identity_insert videostore.Employee on").ExecuteUpdate();
            _session.CreateSQLQuery("insert into videostore.Employee(Id, First, Last, DateHired, DateOfBirth, Username, Password, Store_Id, IsTopLevel) values (0, 'Ryan', 'Mcfall', '2018-11-12', '1860-11-12', 'rmcfall', 'Abc123$!', 0, 1)").ExecuteUpdate();
            _session.CreateSQLQuery("set identity_insert videostore.Employee off").List();
            _session.GetCurrentTransaction().Commit();
            super = _session.Get<Employee>(0);
            testStore = _session.Get<Store>(0);
            /*var emp = new Employee()
            {
                Name = new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                },
                DateHired = DateTime.Now,
                DateOfBirth = new DateTime(1999, 8, 11),
                Username = "rymcFall",
                Password = "Ab$3456789",
                Store = new Store()
                {
                    StoreName = "Last Store",
                    StreetAddress = "1234 Broke St",
                    PhoneNumber = "616-666-0000",
                    ZipCode = new ZipCode()
                    {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                }

            };*/
            new PersistenceSpecification<Employee>(_session, new DateEqualityComparer())
                .CheckProperty(e => e.Name, new Name
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                })
                .CheckProperty(e => e.DateHired, new DateTime(2018, 1, 12))
                .CheckProperty(e => e.DateOfBirth, new DateTime(1860, 6, 9))
                .CheckProperty(e => e.Username, "ronjonmcfall")
                .CheckProperty(e => e.Password, "Abc123$!")
                .CheckReference(e => e.Store, testStore)
                .CheckReference(e => e.Supervisor, super)
                .VerifyTheMappings();
        }


        /** A-Level
         * employee - supervisor
         * */

    }
}