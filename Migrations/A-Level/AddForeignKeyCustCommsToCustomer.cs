﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(35)]
    public class AddForeignKeyCustCommsToCustomer : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("CustCommsToCustomer")
                            .OnTable("CustComms")
                            .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("CustCommsToCustomer")
                            .FromTable("CustComms")
                            .InSchema("videostore")
                            .ForeignColumn("Customer_Id")
                            .ToTable("Customer")
                            .InSchema("videostore")
                            .PrimaryColumn("Id")
                            .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
