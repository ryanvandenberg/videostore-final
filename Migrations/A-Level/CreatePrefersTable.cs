﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(19)]
    public class CreatePrefersTable : Migration
    {
        public override void Down()
        {
            Delete.Table("Prefers")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Prefers")
                .InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Customer_Id").AsInt32().NotNullable()
                .WithColumn("Store_Id").AsInt32().NotNullable()
                .WithColumn("PrefersOrder").AsInt32().NotNullable();
        }
    }
}
