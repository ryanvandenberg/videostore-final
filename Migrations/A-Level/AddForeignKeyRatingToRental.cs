﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(38)]
    public class AddForeignKeyRatingToRental : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("RatingToRental")
               .OnTable("Rating")
               .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("RatingToRental")
               .FromTable("Rating")
               .InSchema("videostore")
               .ForeignColumn("Rental_Id")
               .ToTable("Rental")
               .InSchema("videostore")
               .PrimaryColumn("Id")
               .OnUpdate(System.Data.Rule.Cascade);
        }

    }
}
