﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(36)]
    public class AddForeignKeyCustCommsToCommunication : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("CustCommsToCommunication")
                            .OnTable("CustComms")
                            .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("CustCommsToCommunication")
                .FromTable("CustComms")
                .InSchema("videostore")
                .ForeignColumn("CommunicationMethod_Id")
                .ToTable("CommunicationMethod")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDelete(System.Data.Rule.None)
                .OnUpdate(System.Data.Rule.None);
        }
    }
}
