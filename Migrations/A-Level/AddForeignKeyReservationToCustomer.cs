﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(40)]
    public class AddForeignKeyReservationToCustomer : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("ReservationToCustomer")
                           .OnTable("Reservation")
                           .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("ReservationToCustomer")
               .FromTable("Reservation")
               .InSchema("videostore")
               .ForeignColumn("Customer_Id")
               .ToTable("Customer")
               .InSchema("videostore")
               .PrimaryColumn("Id")
               .OnUpdate(System.Data.Rule.Cascade);
        }
    }
}
