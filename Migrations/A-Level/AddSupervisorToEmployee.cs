﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(41)]
    public class AddSupervisorToEmployee : Migration
    {
        public override void Down()
        {
            Delete.Column("Supervisor_Id")
                .FromTable("Employee")
                .InSchema("videostore");
            Delete.Column("IsTopLevel")
                .FromTable("Employee")
                .InSchema("videostore");
        }

        public override void Up()
        {

            Alter.Table("Employee")
                .InSchema("videostore")
                .AddColumn("Supervisor_Id").AsInt32().Nullable()
                .AddColumn("IsTopLevel").AsInt32().Nullable().WithDefaultValue(0);
        }

    }
}
