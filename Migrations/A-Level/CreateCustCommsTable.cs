﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(34)]
    public class CreateCustCommsTable : Migration
    {
        public override void Down()
        {
            Delete.Table("CustComms")
                          .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("CustComms")
                           .InSchema("videostore")
                           .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                           .WithColumn("Customer_Id").AsInt32().NotNullable()
                           .WithColumn("CommunicationMethod_Id").AsInt32().NotNullable();
                           //.WithColumn("PrefersOrder").AsInt32().NotNullable();
        }
    }
}
