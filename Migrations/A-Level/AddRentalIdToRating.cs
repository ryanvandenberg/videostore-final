﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(37)]
    public class AddRentalIdToRating : Migration
    {
        public override void Down()
        {
            Delete.Column("Rental_Id")
                .FromTable("Rating")
                .InSchema("videostore");
        }

        public override void Up()
        {

            Alter.Table("Rating")
                .InSchema("videostore")
                .AddColumn("Rental_Id").AsInt32().Nullable();
        }

    }
}
