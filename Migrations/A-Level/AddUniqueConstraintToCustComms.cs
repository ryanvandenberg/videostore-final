﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(43)]
    public class AddUniqueConstraintToCustComms : Migration
    {
        public override void Down()
        {
            Delete.UniqueConstraint("CustCommsUnique")
                .FromTable("CustComms")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.UniqueConstraint("CustCommsUnique")
                .OnTable("CustComms")
                .WithSchema("videostore")
                .Columns("Customer_Id", "CommunicationMethod_Id");
        }
    }
}
