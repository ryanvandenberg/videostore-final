﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(42)]
    public class AddForeignKeySupervisorToEmployee : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("EmpToSup")
               .OnTable("Employee")
               .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("EmpToSup")
               .FromTable("Employee")
               .InSchema("videostore")
               .ForeignColumn("Supervisor_Id")
               .ToTable("Employee")
               .InSchema("videostore")
               .PrimaryColumn("Id")
               .OnUpdate(System.Data.Rule.None);
        }
    }
}
