﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(32)]
    public class AddCustomerToRental : Migration
    {
        public override void Down()
        {
            Delete.Column("Customer_Id")
                .FromTable("Rental")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Rental")
               .InSchema("videostore")
               .AddColumn("Customer_Id").AsInt32().NotNullable();
        }


    }
}
