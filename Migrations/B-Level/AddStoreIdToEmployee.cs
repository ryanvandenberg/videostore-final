﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(31)]
    public class AddStoreIdToEmployee : Migration
    {
        public override void Down()
        {
            Delete.Column("Store_Id")
                .FromTable("Employee")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Employee")
                .InSchema("videostore")
                .AddColumn("Store_Id").AsInt32().NotNullable();
        }
    }
}
