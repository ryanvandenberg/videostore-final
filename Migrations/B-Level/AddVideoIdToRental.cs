﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(30)]
    public class AddVideoIdToRental : Migration
    {
        public override void Down()
        {
            Delete.Column("Video_Id")
                .FromTable("Rental")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Rental")
                .InSchema("videostore")
                .AddColumn("Video_Id").AsInt32().NotNullable();
        }
    }
}
