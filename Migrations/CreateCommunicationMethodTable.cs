﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(24)]
    public class CreateCommunicationMethodTable : Migration
    {
        public override void Down()
        {
            Delete.Table("CommunicationMethod").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("CommunicationMethod")
               .InSchema("videostore")
               .WithColumn("Id").AsInt32().Identity().PrimaryKey()
               .WithColumn("Name").AsString().NotNullable()
               .WithColumn("Frequency").AsInt32().NotNullable()
               .WithColumn("Units").AsString().NotNullable();
        }
    }
}
