﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(25)]
    public class CreateRatingTable : Migration
    {
        public override void Down()
        {
            Delete.Table("Rating").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Rating")
                           .InSchema("videostore")
                           .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                           .WithColumn("Score").AsInt32().NotNullable()
                           .WithColumn("Comment").AsString().NotNullable();        
        }
    }
}
