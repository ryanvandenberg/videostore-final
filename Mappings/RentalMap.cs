﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class RentalMap : ClassMap<Rental>
    {

        public RentalMap()
        {
            Id(e => e.Id);
            Map(r => r.RentalDate);
            Map(r => r.DueDate);
            Map(r => r.ReturnDate);
            References<Video>(r => r.Video).Cascade.All();
            References<Customer>(r => r.Customer).Cascade.All();
            HasOne<Rating>(r => r.Rating).PropertyRef(c => c.Rental).Cascade.All();

        }
    }
}
