﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class CommunicationMethodMap : ClassMap<CommunicationMethod>
    {
        public CommunicationMethodMap()
        {
            Id(c => c.Id);
            Map(c => c.Name);
            Map(c => c.Frequency);
            Map(c => c.Units);
            HasManyToMany(c => c.Customers).Inverse()
                .Table("CustComms")
                .Cascade.All();
        }
    }
}
