﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class EmployeeMap : ClassMap<Employee>
    {

        public EmployeeMap()
        {
            Id(e => e.Id);
            Component(e => e.Name, m => {
                m.Map(n => n.Title);
                m.Map(n => n.First);
                m.Map(n => n.Middle);
                m.Map(n => n.Last);
                m.Map(n => n.Suffix);
            });
            Map(e => e.DateHired);
            Map(e => e.DateOfBirth);
            Map(e => e.Username);
            Map(e => e.Password);
            References<Store>(e => e.Store).Cascade.All();
            References<Employee>(e => e.Supervisor).Cascade.All();
            //add is manager
            //add supervisor relation
        }
    }
}
