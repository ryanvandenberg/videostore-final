﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    class GenreMap : ClassMap<Genre>
    {
        public GenreMap()
        {
            Table("Genres");
            Schema("imdb");
            Id(g => g.Name, "Genre").GeneratedBy.Assigned();


        }
    }
}
