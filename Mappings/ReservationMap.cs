﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class ReservationMap : ClassMap<Reservation>
    {

        public ReservationMap()
        {
            Id(r => r.Id);
            Map(r => r.ReservationDate);
            References<Movie>(r => r.Movie, "Movie_Id");
            References<Customer>(r => r.Customer).Cascade.All();
        }
    }
}
