﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;


namespace Mappings
{
    public class RatingMap : ClassMap<Rating>
    {

        public RatingMap()
        {
            Id(r => r.Id);
            Map(r => r.Score);
            Map(r => r.Comment);
            References<Rental>(r => r.Rental).Cascade.All();
        }
    }
}
